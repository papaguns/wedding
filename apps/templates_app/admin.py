from django.contrib import admin

from apps.templates_app.models import (
    Invite, Decline, Counter, TelegramChatBot, SiteSettings
)


@admin.register(Invite)
class InviteAdmin(admin.ModelAdmin):
    class Meta:
        model = Invite


@admin.register(Decline)
class DeclineAdmin(admin.ModelAdmin):
    class Meta:
        model = Decline


@admin.register(Counter)
class CounterAdmin(admin.ModelAdmin):
    class Meta:
        model = Counter


@admin.register(TelegramChatBot)
class TelegramAdmin(admin.ModelAdmin):
    class Meta:
        model = TelegramChatBot


@admin.register(SiteSettings)
class SiteSettingsAdmin(admin.ModelAdmin):
    class Meta:
        model = SiteSettings
