from django.db import models


class Invite(models.Model):
    class Meta:
        verbose_name = '1. Приглашение'
        verbose_name_plural = '1. Приглашения'

    guest = models.CharField(
        verbose_name='ФИО',
        max_length=255,
        null=False, blank=False,
    )
    adults = models.PositiveIntegerField(
        verbose_name='Взрослые',
        help_text='Количество взрослых',
        null=False,
        blank=False,
    )
    children = models.PositiveIntegerField(
        verbose_name='Дети',
        help_text='Количество детей',
        null=False,
        blank=False,
    )
    comment = models.TextField(
        verbose_name='Комментарий',
        null=True, blank=True,
    )

    def __str__(self):
        return self.guest


class Decline(models.Model):
    class Meta:
        verbose_name = '2. Отказ'
        verbose_name_plural = '2. Отказы'

    guest = models.CharField(
        verbose_name='ФИО',
        max_length=255,
        null=False, blank=False,
    )
    comment = models.TextField(
        verbose_name='Комментарий',
        null=True, blank=True,
    )

    def __str__(self):
        return self.guest


class Counter(models.Model):
    class Meta:
        verbose_name = '3. Просмотры'
        verbose_name_plural = '3. Просмотры'

    count = models.IntegerField(verbose_name='Просмотры', default=0)

    def __str__(self):
        if self.count:
            return str(self.count)


class TelegramChatBot(models.Model):
    class Meta:
        verbose_name = '4. Telegram Бот'
        verbose_name_plural = '4. Telegram Бот'

    token = models.CharField(
        verbose_name='Telegram Бот Токен',
        max_length=255,
    )
    group_id = models.CharField(
        verbose_name='Group ID',
        max_length=255,
    )

    def __str__(self):
        return f'Telegram чат : {self.token}'


class SiteSettings(models.Model):
    class Meta:
        verbose_name = '5. Домен'
        verbose_name_plural = '5. Домен'

    domain = models.CharField(
        max_length=255,
        verbose_name='Домен',
    )

    def __str__(self):
        return self.domain
