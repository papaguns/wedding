from django.urls import reverse

import telebot
import logging
from telebot import types
from telebot.apihelper import ApiException


bot = telebot.TeleBot('')

logging.basicConfig(
    filename='telegram_bot.log',
    level=logging.INFO,
    format='%(asctime)s %(message)s',
    datefmt='%m/%d/%Y/ %I:%M:%S %p',
)


def create_bot(token):
    return telebot.TeleBot(token)


def telegramm_bot(request, chat_id, instance, bot):
    if hasattr(instance, 'adults'):
        message = 'Добрый день, поступил ответ \n От: %s,\n Взрослых: %s,\n ' \
                  'Детей: %s,\n Комментарий: %s\n ' % (instance.guest,
                                                       instance.adults,
                                                       instance.children,
                                                       instance.comment,
                                                       )
    else:
        message = 'Добрый день, поступил отказ \n От: %s,\n Комментарий: %s\n ' % (
            instance.guest,
            instance.comment,
        )
    send_telegram_notification_url(
        message=message,
        group_id=chat_id,
        link=request.build_absolute_uri(
            reverse('admin:templates_app_invite_changelist')
        ),
        bot=bot
    )


@bot.message_handler(commands=['url'], )
def send_telegram_notification_url(message, group_id, link, bot):
    markup = types.InlineKeyboardMarkup()
    button_to_site = types.InlineKeyboardButton(text='перейти', url=link)
    markup.add(button_to_site)
    try:
        bot.send_message(
            group_id,
            message,
            reply_markup=markup,
            parse_mode="Markdown"
        )
    except ApiException:
        _send_telegram_notification(chat_id=group_id, message=message)
        logging.info('Telegram API Exception not valid link')


def _send_telegram_notification(chat_id: str, message: str):
    try:
        bot.send_message(chat_id=chat_id, text=message, parse_mode="Markdown")
    except ApiException:
        logging.info("Telegram API Exception: ")
    except Exception as e:
        logging.error(e)
