from django.urls import path

from apps.templates_app.views import IndexView, ProgramView, FormPageView, \
    FormPageTwoView, RefuseView, AcceptView

urlpatterns = [
    path('accept/', AcceptView.as_view(), name='accept'),
    path('refuse/', RefuseView.as_view(), name='refuse'),
    path('form-page-two/', FormPageTwoView.as_view(), name='form_page_two'),
    path('form-page/', FormPageView.as_view(), name='form_page'),
    path('program/', ProgramView.as_view(), name='program_page'),
    path('', IndexView.as_view(), name='index_page'),
]
