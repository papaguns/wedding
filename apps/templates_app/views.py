from django.shortcuts import redirect, render
from django.urls import reverse_lazy, reverse
from django.views.generic import TemplateView, CreateView
from apps.templates_app.models import (
    Invite, TelegramChatBot, SiteSettings, Decline, Counter
)

from .services import create_bot, telegramm_bot


class IndexView(TemplateView):

    def get(self, request, *args, **kwargs):
        SiteSettings.objects.get_or_create(
                domain=request.build_absolute_uri(
                    reverse('index_page')
                )
            )
        Counter.objects.get_or_create()
        counter = Counter.objects.first()
        counter.count += 1
        counter.save(update_fields=['count'])
        return render(request, 'pages/index.html')


class ProgramView(TemplateView):
    template_name = 'pages/program-page.html'


class FormPageView(CreateView):
    template_name = 'pages/form-page.html'
    model = Invite
    fields = ['guest', 'adults', 'children', 'comment']
    success_url = reverse_lazy('accept')

    def form_valid(self, form):
        invite = form.save(commit=False)
        invite.guest = form.cleaned_data['guest']
        invite.adults = form.cleaned_data['adults']
        invite.children = form.cleaned_data['children']
        invite.comment = form.cleaned_data['comment']
        invite.save()
        telegram = TelegramChatBot.objects.first()
        if telegram:
            group_id = telegram.group_id
            telegramm_bot(
                self.request,
                group_id,
                invite,
                create_bot(telegram.token),
            )
        return redirect(self.success_url)


class FormPageTwoView(CreateView):
    template_name = 'pages/form-page-two.html'
    model = Decline
    fields = ['guest', 'comment']
    success_url = reverse_lazy('refuse')

    def form_valid(self, form):
        invite = form.save(commit=False)
        invite.guest = form.cleaned_data['guest']
        invite.comment = form.cleaned_data['comment']
        invite.save()
        telegram = TelegramChatBot.objects.first()
        if telegram:
            group_id = telegram.group_id
            telegramm_bot(
                self.request,
                group_id,
                invite,
                create_bot(telegram.token),
            )
        return redirect(self.success_url)


class RefuseView(TemplateView):
    template_name = 'pages/refuse.html'


class AcceptView(TemplateView):
    template_name = 'pages/accept.html'
